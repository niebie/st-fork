#!/usr/bin/python3.4

import sys
import os.path
import re                       # regexes

# DEBUGGING ARG PASSING
# print('Number of arguments:', len(sys.argv), 'arguments.')
# print('Argument List:', str(sys.argv))

# We only allow for one argument (no options are valid right now)
if len(sys.argv) == 0:
    sys.exit("No file to convert.")
elif len(sys.argv) > 2:
    sys.exit("Incorrect arguments.")
else:
    #check that the file exists
    if not os.path.isfile(sys.argv[1]):
        sys.exit("Invalid file provided.")
    with open(sys.argv[1]) as to_import:
        lines = to_import.read().splitlines()
    if not lines:
        sys.exit("File is empty.")
    count = 0
    regex = re.compile('^.*?\*color(\d+)\s*\:\s*(\#[0-9a-fA-F]{6})$')
    reg_fore = re.compile('^.*?\*foreground\s*\:\s*(\#[0-9a-fA-F]{6})$')
    reg_back = re.compile('^.*?\*background\s*\:\s*(\#[0-9a-fA-F]{6})$')
    color_dic = {}
    background = "#000000"
    foreground = "grey90"
    for line in lines:
        # if the line does not contain *color##:, or *(back|fore)ground, ignore
        if line:
            match = regex.match(line)
            if match:
                color_dic[int(match.group(1))] = match.group(2)
                # print("{0} : {1}".format(match.group(1), match.group(2)))
            else:
                # check for fore/back ground
                match = reg_fore.match(line)
                if match:
                    foreground = match.group(1)
                else:
                    match = reg_back.match(line)
                    if match:
                        background = match.group(1)

    # debugging
    # print("bg : {0}".format(background))
    # print("fg : {0}".format(foreground))

    # for key in color_dic:
    #     print("{0} : {1}".format(key, color_dic[key]))

    # now that we got the colors, write all of them to a new color header
    out_lines = []
    out_lines.append("static const char *colorname[] = {")
    for i in range(0, 16):
        val = color_dic.get(i)
        if val is not None:
            out_lines.append('  "{0}",'.format(val))
        else:
            out_lines.append('  "#000000",')

    out_lines.append('  [255] = 0,')
    out_lines.append('  "#cccccc",')
    out_lines.append('  "#555555",')

    out_lines.append('  "{0}",'.format(background))
    out_lines.append('  "{0}",'.format(foreground))

    out_lines.append("};");

    out_lines.append("static unsigned int defaultfg = 259;")
    out_lines.append("static unsigned int defaultbg = 258;")
    out_lines.append("static unsigned int defaultcs = 256;")
    out_lines.append("static unsigned int defaultrcs = 257;")

    # debugging
    # for out_line in out_lines:
    #     print(out_line)

    with open("./src/def/color.{0}.h".format\
              (os.path.basename(sys.argv[1])), 'w') as out_file:
        for out_line in out_lines:
            out_file.write("%s\n" % out_line)
