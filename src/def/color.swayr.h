static const char *colorname[] = {
  "#1c1709",
  "#8e4317",
  "#787200",
  "#945c00",
  "#315094",
  "#5c2e40",
  "#00617d",
  "#c2b9a1",
  "#4f4939",
  "#f07935",
  "#d9d138",
  "#ffab26",
  "#8aa9ed",
  "#ff8cb8",
  "#43bfe0",
  "#f2e8c9",
  [255] = 0,
  "#cccccc",
  "#555555",
  "#1c1709",
  "#c2b9a1",
};
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
