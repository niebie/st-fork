static const char *colorname[] = {
  "#000000",
  "#ff0000",
  "#33ff00",
  "#ff0099",
  "#0066ff",
  "#cc00ff",
  "#00ffff",
  "#d0d0d0",
  "#808080",
  "#ff9900",
  "#404040",
  "#606060",
  "#c0c0c0",
  "#e0e0e0",
  "#3300ff",
  "#ffffff",
  [255] = 0,
  "#cccccc",
  "#555555",
  "#000000",
  "#f3f3f3",
};
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
