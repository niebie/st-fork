/* these are the default colors for st. */
static const char *colorname[] = {
  "black",
  "red3",
  "green3",
  "yellow3",
  "blue2",
  "magenta3",
  "cyan3",
  "gray90",

  /* 8 bright colors */
  "gray50",
  "red",
  "green",
  "yellow",
  "#5c5cff",
  "magenta",
  "cyan",
  "white",

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#000000",
  "#eeeeee",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
static unsigned int defaultfg = 256;
static unsigned int defaultbg = 257;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
