static const char *colorname[] = {
  "#000000",
  "#55ff55",
  "#00cc00",
  "#00cc00",
  "#005500",
  "#55ff55",
  "#00cc00",
  "#00cc00",
  "#000000",
  "#55ff55",
  "#55ff55",
  "#55ff55",
  "#005500",
  "#55ff55",
  "#55ff55",
  "#00cc00",
  [255] = 0,
  "#cccccc",
  "#555555",
  "#000000",
  "#00cc00",
};
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
