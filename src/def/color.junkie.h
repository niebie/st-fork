static const char *colorname[] = {
  "#2E3436",
  "#CC0000",
  "#73E409",
  "#C4A000",
  "#3465A4",
  "#ff17ae",
  "#06989A",
  "#D3D7CF",
  "#555753",
  "#EF2929",
  "#8AE234",
  "#FCE94F",
  "#729FCF",
  "#AD7FA8",
  "#34E2E2",
  "#EEEEEC",
  [255] = 0,
  "#cccccc",
  "#555555",
  "#2B2B2B",
  "#DEDEDE",
};
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
