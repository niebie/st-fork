static const char *colorname[] = {
  "#2a1d17",
  "#da1657",
  "#3ea250",
  "#e3d33d",
  "#3ea290",
  "#ff850d",
  "#8c16da",
  "#e9e9e9",
  "#4f362b",
  "#da4375",
  "#6cb87a",
  "#e3da84",
  "#8ca8a3",
  "#ffa64f",
  "#a167c7",
  "#fdfdfd",
  [255] = 0,
  "#cccccc",
  "#555555",
  "#2a1d17",
  "#e9e9e9",
};
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
