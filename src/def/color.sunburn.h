/* from http://www.xcolors.net/dl/Fulminated-Sunburn */
static const char *colorname[] = {

  /* dark colors */
  /* black */
  "#110900",
  /* red */
  "#fe4400",
  /* green */
  "#ff9a00",
  /* yellow */
  "#ef6b18",
  /* blue */
  "#c14618",
  /* magenta */
  "#CC0000",
  /* cyan */
  "#A87B58",
  /* white */
  "#f0b17e",

  /* light colors */
  /* black */
  "#723d02",
  /* red */
  "#fe4400",
  /* green */
  "#fe4400",
  /* yellow */
  "#fe4400",
  /* blue */
  "#814618",
  /* magenta */
  "#fe4400",
  /* cyan */
  "#fe4400",
  /* white */
  "#e77d29",

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#cccccc",
  "#555555",

  "#110900",
  "#fe7000",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
