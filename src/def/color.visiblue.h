static const char *colorname[] = {
  "#333366",
  "#6666CC",
  "#0099CC",
  "#3366CC",
  "#006699",
  "#0066FF",
  "#669999",
  "#99CCCC",
  "#333399",
  "#9999FF",
  "#00CCFF",
  "#6699FF",
  "#0099CC",
  "#0099FF",
  "#66CCCC",
  "#CCFFFF",
  [255] = 0,
  "#cccccc",
  "#555555",
  "#000000",
  "#666699",
};
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
