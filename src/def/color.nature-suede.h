static const char *colorname[] = {
  "#392925",
  "#98724C",
  "#908F32",
  "#AA964C",
  "#7B854E",
  "#6B5644",
  "#5C5142",
  "#C8B55B",
  "#544B2E",
  "#AF652F",
  "#C3C13D",
  "#C8B55B",
  "#70A16C",
  "#98724C",
  "#778725",
  "#E4DC8C",
  [255] = 0,
  "#cccccc",
  "#555555",
  "#170F0D",
  "#746C48",
};
static unsigned int defaultfg = 259;
static unsigned int defaultbg = 258;
static unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
